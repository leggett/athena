
#include "TrigFTK_RawDataAlgs/FTK_RDO_CreatorAlgo.h"
#include "TrigFTK_RawDataAlgs/FTK_RDO_ReaderAlgo.h"
#include "../FTK_RDO_MonitorAlgo.h"

DECLARE_COMPONENT( FTK_RDO_CreatorAlgo )
DECLARE_COMPONENT( FTK_RDO_ReaderAlgo )
DECLARE_COMPONENT( FTK_RDO_MonitorAlgo )
